Fonts are embedded on the PowerPoint and should be installed automatically when you open the pptx file.
However : 
- If you are on a Mac, it may not install so please install fonts in the folder Fonts
- If you don't have admin Rights on your computer, you might not be able to install any fonts, please use the System Fonts variant of the template (using Arial)
